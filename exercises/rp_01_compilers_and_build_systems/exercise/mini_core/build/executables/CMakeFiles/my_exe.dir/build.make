# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.18

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core"

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build"

# Include any dependencies generated for this target.
include executables/CMakeFiles/my_exe.dir/depend.make

# Include the progress variables for this target.
include executables/CMakeFiles/my_exe.dir/progress.make

# Include the compile flags for this target's objects.
include executables/CMakeFiles/my_exe.dir/flags.make

executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o: executables/CMakeFiles/my_exe.dir/flags.make
executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o: ../executables/many_object_in_stack_example.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir="/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/CMakeFiles" --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o"
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o -c "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/executables/many_object_in_stack_example.cpp"

executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.i"
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/executables/many_object_in_stack_example.cpp" > CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.i

executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.s"
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" && /usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/executables/many_object_in_stack_example.cpp" -o CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.s

# Object files for target my_exe
my_exe_OBJECTS = \
"CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o"

# External object files for target my_exe
my_exe_EXTERNAL_OBJECTS =

executables/my_exe: executables/CMakeFiles/my_exe.dir/many_object_in_stack_example.cpp.o
executables/my_exe: executables/CMakeFiles/my_exe.dir/build.make
executables/my_exe: src/libcore_library.so
executables/my_exe: executables/CMakeFiles/my_exe.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir="/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/CMakeFiles" --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX executable my_exe"
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/my_exe.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
executables/CMakeFiles/my_exe.dir/build: executables/my_exe

.PHONY : executables/CMakeFiles/my_exe.dir/build

executables/CMakeFiles/my_exe.dir/clean:
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" && $(CMAKE_COMMAND) -P CMakeFiles/my_exe.dir/cmake_clean.cmake
.PHONY : executables/CMakeFiles/my_exe.dir/clean

executables/CMakeFiles/my_exe.dir/depend:
	cd "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build" && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core" "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/executables" "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build" "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables" "/home/gianmarchito/Documenti/Programmazione/Robot Programming/robotprogramming_2021_22/exercises/rp_01_compilers_and_build_systems/exercise/mini_core/build/executables/CMakeFiles/my_exe.dir/DependInfo.cmake" --color=$(COLOR)
.PHONY : executables/CMakeFiles/my_exe.dir/depend

